import { Line } from 'vue-chartjs'
export default {
  extends: Line,
  watch: {
    update() {
      this.$data._chart.update()
    },
  },
  props: ['chartData', 'options', 'update'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  },
}

// this ._chart is a private variable
