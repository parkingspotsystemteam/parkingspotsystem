import { PolarArea } from 'vue-chartjs'

export default {
  extends: PolarArea,
  name: 'PolarArea',
  watch: {
    update() {
      this.$data._chart.update()
    },
  },
  props: ['chartData', 'options', 'update'],
  mounted() {
    this.renderChart(this.chartData, this.options)
  },
}
