export default {
  role_admin: 'admin',
  role_user: 'user',
  role_guest: 'guest',

  default_role() {
    return this.role_guest
  },

  getRoleIndex(forRole) {
    switch (forRole) {
      case this.role_admin:
        return 2
      case this.role_user:
        return 1
      case this.role_guest:
        return 0
      default:
        return null
    }
  },

  getIndexRole(index) {
    switch (index) {
      case 0:
        return this.role_guest
      case 1:
        return this.role_user
      case 2:
        return this.role_admin
      default:
        return null
    }
  },
}
