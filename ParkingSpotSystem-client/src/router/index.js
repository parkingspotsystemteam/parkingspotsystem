import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

import Roles from '../lib/roles'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

async function userHasRole(expected) {
  const role = await Vue.prototype.$user.getRole()
  return Roles.getRoleIndex(role) >= expected
}

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE,
  })

  Router.beforeEach(async (to, from, next) => {
    console.log(Vue.prototype.$auth.currentUser || 'User not logged in.')
    if (to.matched.some((record) => record.meta.role)) {
      const maxNeeded =
        to.matched
          .map((r) => Roles.getRoleIndex(r.meta.role))
          .reduce((acc, curr) => {
            // Neki elementi su null, probaj izracunati maksimum, ako to ne radi
            // probaj koristiti akumuliranu vrijednost, ako to ne radi nadaj se da
            // je curr definiran.
            return Math.max(acc, curr) || acc || curr
          }) || 0 // Ako putanja nema postavljen role, pretpostavi da pristup nije
      // ogranicen.
      if (await userHasRole(maxNeeded)) {
        next()
      } else {
        next('accessDenied')
      }
    } else {
      next()
    }
  })
  return Router
}
