import Roles from '../lib/roles'

const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/Index.vue') }],
  },
  {
    path: '/about',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/About.vue') }],
  },
  {
    path: '/accessDenied',
    component: () => import('layouts/MainLayout.vue'),
    children: [{ path: '', component: () => import('pages/AccessDenied.vue') }],
  },
  {
    path: '/administration',
    component: () => import('layouts/MainLayout.vue'),
    meta: { role: Roles.role_admin },
    children: [
      {
        path: '',
        component: () => import('pages/Administration.vue'),
      },
      {
        path: '/onlineUsers',
        component: () => import('pages/ParkingSpotSystem/OnlineUsersIndex.vue'),
      },
      {
        path: '/usersInfo',
        component: () => import('pages/ParkingSpotSystem/UsersInfoIndex.vue'),
      },
      {
        path: '/home',
        component: () => import('pages/ParkingSpotSystem/HomeIndex.vue'),
      },
    ],
  },
  {
    path: '/login',
    component: () => import('layouts/LoginPageLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login/LoginIndex.vue') },
    ],
  },
  {
    path: '/users',
    component: () => import('layouts/MainLayout.vue'),
    meta: { role: Roles.role_user },
    children: [
      {
        path: '/parkingLocation',
        component: () => import('pages/ParkingSpotSystem/ParkingLocation.vue'),
      },
      {
        path: '/parkings',
        component: () => import('pages/ParkingSpotSystem/ParkingsIndex.vue'),
      },
      {
        path: '/recension',
        component: () => import('pages/ParkingSpotSystem/RecensionIndex.vue'),
      },
      {
        path: '/request1',
        component: () => import('pages/ParkingSpotSystem/Request1Index.vue'),
      },
      {
        path: '/ticket',
        component: () => import('pages/ParkingSpotSystem/Ticket.vue'),
      },
      {
        path: '/request2',
        component: () => import('pages/ParkingSpotSystem/Request2Index.vue'),
      },
      /* {
        path:  '/contactList',
        component: () => import('pages/ParkingSportSystem/ContactList.vue')
      }, */
      {
        path: '/home',
        component: () => import('pages/ParkingSpotSystem/HomeIndexUser.vue'),
      },
    ],
  },
]

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  })
}

export default routes
