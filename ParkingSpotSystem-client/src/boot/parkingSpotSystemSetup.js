import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
import * as VueGoogleMaps from 'vue2-google-maps'

import Roles from '../lib/roles'

const firebaseConfig = {
  apiKey: 'AIzaSyBG_tIB6luX7gtUaHbTkIDAghzPi0VoJgo',
  authDomain: 'parkingspotsystem.firebaseapp.com',
  databaseURL:
    'https://parkingspotsystem-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'parkingspotsystem',
  storageBucket: 'parkingspotsystem.appspot.com',
  messagingSenderId: '1095533324862',
  appId: '1:1095533324862:web:261b2ad61ebe272ed77f6f',
  measurementId: 'G-R2E2MZM9TZ',
}

firebase.initializeApp(firebaseConfig)

import Vue from 'vue'

Vue.prototype.$auth = firebase.auth()
Vue.prototype.$db = firebase.firestore()
Vue.prototype.$storage = firebase.storage()

Vue.prototype.$db.deleteQueryBatch = async function (query) {
  const snapshot = await query.get()

  const batchSize = snapshot.size
  if (batchSize === 0) {
    return
  }

  // Delete documents in a batch
  const batch = Vue.prototype.$db.batch()
  snapshot.docs.forEach((doc) => {
    batch.delete(doc.ref)
  })
  await batch.commit()

  // Recurse on the next process tick, to avoid
  // exploding the stack.
  process.nextTick(() => {
    Vue.prototype.$db.deleteQueryBatch(query)
  })
}

Vue.prototype.$db.deleteCollection = async function (
  collectionPath,
  batchSize
) {
  const collectionRef = Vue.prototype.$db.collection(collectionPath)
  const query = collectionRef.orderBy('__name__').limit(batchSize || 20)

  await Vue.prototype.$db.deleteQueryBatch(query)
}

const userUtil = {
  getUser: () => {
    return new Promise((resolve, reject) => {
      const unsubscribe = firebase.auth().onAuthStateChanged(
        (userAuth) => {
          resolve(userAuth)
          unsubscribe()
        },
        (err) => {
          console.error(err)
          resolve(null)
          unsubscribe()
        }
      )
    })
  },
  isLoggedIn: () => {
    return new Promise((resolve, reject) => {
      userUtil
        .getUser()
        .then((user) => {
          resolve(user != null)
        })
        .catch((err) => {
          console.error(err)
          resolve(false)
        })
    })
  },
  getRole: () => {
    return new Promise((resolve, reject) => {
      userUtil
        .getUser()
        .then((user) => {
          if (user == null) {
            resolve(Roles.role_guest)
          } else {
            if (user.email.endsWith('@student.uniri.hr')) {
              resolve(Roles.role_admin)
            } else {
              resolve(Roles.role_user)
            }
          }
        })
        .catch((err) => {
          console.error(err)
          resolve(Roles.role_guest)
        })
    })
  },
}

Vue.prototype.$user = userUtil

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAd9FgCMmKHq4szxzjwarXzCwA-9lDHN70',
  },
  installComponents: true,
})
