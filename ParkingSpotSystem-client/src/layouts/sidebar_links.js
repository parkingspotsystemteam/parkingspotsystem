import Roles from '../lib/roles'

const SidebarLinks = {
  sidebar_guest: [
    {
      title: 'Free parking spots',
      caption: 'Number of free parking spots in your aeria',
      icon: 'directions_car',
      link: 'counter',
    },
    {
      title: 'About',
      caption: 'Read about our mission',
      icon: 'info',
      link: 'about',
    },
  ],

  sidebar_admin: [
    {
      title: 'Administration',
      caption: 'Number of free parking spots in your aeria',
      icon: 'directions_car',
      link: 'administration',
    },
    {
      title: 'Information about users',
      caption: 'Information about users who are in the database',
      icon: 'people',
      link: 'usersInfo',
    },
    {
      title: 'Online users',
      caption: 'List of online users',
      icon: 'list',
      link: 'onlineUsers',
    },
  ],

  sidebar_user: [
    {
      title: 'Locations of Parkings',
      caption: 'Locations on Google Maps',
      icon: 'location_on',
      link: 'parkingLocation',
    },
    {
      title: 'Parkings',
      caption: 'Choose your parking and a parking zone',
      icon: 'font_download',
      link: 'parkings',
    },
    {
      title: 'Recension',
      caption: 'Tell us how you feel about our application',
      icon: 'create',
      link: 'recension',
    },
    {
      title: 'Request 1',
      caption: 'Request a parking spot',
      icon: 'drive_eta',
      link: 'request1',
    },
    {
      title: 'Buy a ticket',
      caption: 'Buy a ticket by choosing the parking you want and the zone',
      icon: 'confirmation_number',
      link: 'ticket',
    },
    {
      title: 'Request 2',
      caption: 'Request prolonging of the parking ticket',
      icon: 'confirmation_number',
      link: 'request2',
    },
    {
      title: 'List of Contacts',
      caption: 'Contact us if you have a problem with our application',
      icon: 'font_download',
      link: 'contactList',
    },
    {
      title: 'Home',
      caption: '',
      icon: 'home',
      link: 'home',
    },
  ],

  forRole(role) {
    switch (role) {
      case Roles.role_admin:
        return this.sidebar_admin
      case Roles.role_user:
        return this.sidebar_user
      default:
        return this.sidebar_guest
    }
  },
}

export default SidebarLinks
