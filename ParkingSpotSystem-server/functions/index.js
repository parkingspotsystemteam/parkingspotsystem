/* eslint-disable no-eq-null */
const express = require('express');
const bodyParser = require('body-parser');
//const fuctions = require('firebase-functions');
const app = express();

app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json())

const admin = require("firebase-admin");

var serviceAccount = require("./parkingspotsystem-firebase-adminsdk-n27lt-ae23084e9b.json");
const { query } = require('express');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://parkingspotsystem.firebaseio.com"
});

const db = admin.firestore();

/*
exports.AddUserRole = functions.auth.user().onCreate(async (authUser) => {
    if (authUser.email) {
        let customClaims = {
            role: "user"
        }
        
        if (authUser.email.endsWith("@student.uniri.hr")) {
            customClaims.role = "admin"
        }
        
        try {
            var _ = await admin.auth().setCustomUserClaims(authUser.uid, customClaims)

            return db.collection("roles").doc(authUser.uid).set({
                email: authUser.email,
                role: customClaims
            })
        } catch (error) {
            console.log(error)
        }

    }
});
*/

app.get('/hello',(request,response) => {
    return response.send('Hello world');
});
/*
app.get('/getDateTime', (request, response) => {
    let res = [];
    db.collection('parkingSpot')
        .get()
        .then((querySnapshot) => {
           querySnapshot.forEach((doc) => { 
             let document = {
                 id: doc.id,
                 data: doc.data()
             }
             res.push(document)
           })
           return  response.send(res)
        })
        .catch ((error) => {
            return response.send("Error getting documents: ", error);
        })
})

app.post('/add_date_time', (request,response) => {
    const data = request.body;
    console.log(data.endDateTime);
    return response.send ('POST metoda -> Add ' +data.endDateTime);
});

app.put('/change_end_date_time', (request,response) => {
    const data = request.body;
    console.log(data.endDateTime);
    return response.send('PUT metoda -> Change ' +data.endDateTime);
});

app.delete('/delete_end_date_time', (request, response) => {
    const data = request.body;
    console.log('Delete '+data.endDateTime);
    return response.send('Delete metoda -> Delete ' +data.endDateTime);
});

app.post("/BenefitsCreate", (request,response) => {
    if(Object.keys(request.body).length) {
        db.collection("Benefits").doc().set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
    } else {
        return response.send (
            "No post data for new document. " +
            "A new document is not created!"
        )
    }

})

app.put("/BenefitsUpdate", (request, response) => {
    if(Object.keys(request.body).length){
        if(request.query.id != null){
            db.collection("Benefits")
                .doc(request.query.id)
                .set(request.body)
                .then(()=> {
                    return response.send(
                        "Document sucessfully written - " +
                        "updated!"
                    )
                })
                .catch((error)=> {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " + 
            "A document  is not updated!"
        )
    }
})

app.delete("/BenefitsDelete",(request, response) => {
    if(request.query.id != null) {
        db.collection("Benefits").doc(request.query.id).delete()
            .then(()=> {
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
    } else {
        return response.send(
            "A parameter id is not set. " + 
            "A document is not deleted!"
        )
    }
})
// SpecialBenefits
app.post("/SpecialBenefitsCreate", (request,response) => {
    if(Object.keys(request.body).length) {
        db.collection("SpecialBenefits").doc().set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
    } else {
        return response.send (
            "No post data for new document. " +
            "A new document is not created!"
        )
    }

})

app.put("/SpecialBenefitsUpdate", (request, response) => {
    if(Object.keys(request.body).length){
        if(request.query.id != null){
            db.collection("SpecialBenefits")
                .doc(request.query.id)
                .set(request.body)
                .then(()=> {
                    return response.send(
                        "Document sucessfully written - " +
                        "updated!"
                    )
                })
                .catch((error)=> {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " + 
            "A document  is not updated!"
        )
    }
})

app.delete("/SpecialBenefitsDelete",(request, response) => {
    if(request.query.id != null) {
        db.collection("SpecialBenefits").doc(request.query.id).delete()
            .then(()=> {
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
    } else {
        return response.send(
            "A parameter id is not set. " + 
            "A document is not deleted!"
        )
    }
})

// Ticket

app.post("/TicketCreate", (request,response) => {
    if(Object.keys(request.body).length > 0) {
        db.collection("Ticket").doc().set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
    } else {
        return response.send (
            "No post data for new document. " +
            "A new document is not created!"
        )
    }

})
app.put("/TicketUpdate", (request, response) => {
    if(Object.keys(request.body).length){
        if(request.query.id != null){
            db.collection("Ticket")
                .doc(request.query.id)
                .set(request.body)
                .then(()=> {
                    return response.send(
                        "Document sucessfully written - " +
                        "updated!"
                    )
                })
                .catch((error)=> {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " + 
            "A document  is not updated!"
        )
    }
})

app.delete("/TicketDelete",(request, response) => {
    if(request.query.id != null) {
        db.collection("Ticket").doc(request.query.id).delete()
            .then(()=> {
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
    } else {
        return response.send(
            "A parameter id is not set. " + 
            "A document is not deleted!"
        )
    }
})

// user

app.post("/userCreate", async(request,response) => {
    if(Object.keys(request.body).length > 0) {
        if (request.query.id == null) {
            db.collection("user").doc().set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } else {
            //kreiranje podataka u podkolekciji BankCard
            db.collection("user").doc(request.query.id)
                .collection("BankCard")
                .set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
            //kreiranje podataka u podkolekciji Review
            db.collection("user").doc(request.query.id)
                .collection("Review")
                .set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        }
    } else {
        return response.send(
            "Body is empty. Cannot write new document."
        )
    }
})

app.put("/userUpdate", (request, response) => {
    if(Object.keys(request.body).length) {
        if(request.query.id != null) {
            if ( request.query.subCollection === "BankCard" ) { 
                //ažuriranje podataka u podkolekciji za svaki pojedini dokument
                db.collection("user").doc(request.query.id).collection("BankCard").set(request.body)            
                .then(() => {
                    return response.send(
                        "Document succesfully written - updated!"
                    )
                })
                .catch((error) => {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
            } else if ( request.query.subCollection == null ) {
                db.collection("user").doc(request.query.id).set(request.body)
                    .then(()=> {
                        return response.send(
                            "Document sucessfully written - " +
                            "updated!"
                        )
                    })
                    .catch((error)=> {
                        return response.send(
                            "Error writing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "Sub-collection other than 'BankCard' is not supported."
                )
            }
            // podkolekcija Review
            if ( request.query.subCollection === "Review" ) { 
                //ažuriranje podataka u podkolekciji za svaki pojedini dokument
                db.collection("user").doc(request.query.id).collection("Review").set(request.body)            
                .then(() => {
                    return response.send(
                        "Document succesfully written - updated!"
                    )
                })
                .catch((error) => {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
            } else if ( request.query.subCollection == null ) {
                db.collection("user").doc(request.query.id).set(request.body)
                    .then(()=> {
                        return response.send(
                            "Document sucessfully written - " +
                            "updated!"
                        )
                    })
                    .catch((error)=> {
                        return response.send(
                            "Error writing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "Sub-collection other than 'Review' is not supported."
                )
            }
        } else {
            return response.send(
                "ID is not specified. Cannot update without ID."
            )
        }
    } else {
        return response.send(
            "Body is empty. Cannot update without body."
        )
    }
})

app.delete("/userDelete", (request, response) => {    
    if(request.query.id != null) {
        if (request.query.subCollection == null) {
            db.collection("user").doc(request.query.id).delete()
            .then(()=> {
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
            // podkolekcija BankCard
        } else if (request.query.subCollection === "BankCard") {
            db.collection("user").doc(request.query.id).collection("BankCard").delete()            
            .then(() => {
                return response.send(
                    "Document succesfully written - updated!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } else {
            return response.send(
                "Deleting anything but 'BankCard' subcollection is not supported."
            )
        }
    }
    // podkolekcija Review
        else if (request.query.subCollection === "Review") {
            db.collection("user").doc(request.query.id).collection("Review").delete()            
            .then(() => {
                return response.send(
                    "Document succesfully written - updated!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        }
     else {
        return response.send(
            "Cannot delete document."
        )
    }
})

// Request
app.post("/RequestCreate", async(request,response) => {
    if(Object.keys(request.body).length > 0) {
        if (request.query.id == null) {
            db.collection("Request").doc().set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } else {
            //kreiranje podataka u podkolekciji za svaki pojedini dokument
            db.collection("Request").doc(request.query.id)
                .collection("Counter")
                .set(request.body)
            .then(() => {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        }
    } else {
        return response.send(
            "Body is empty. Cannot write new document."
        )
    }
})

app.put("/RequestUpdate", (request, response) => {
    if(Object.keys(request.body).length) {
        if(request.query.id != null) {
            if ( request.query.subCollection === "Counter" ) { 
                //ažuriranje podataka u podkolekciji za svaki pojedini dokument
                db.collection("Request").doc(request.query.id).collection("Counter").set(request.body)            
                .then(() => {
                    return response.send(
                        "Document succesfully written - updated!"
                    )
                })
                .catch((error) => {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
            } else if ( request.query.subCollection == null ) {
                db.collection("Request").doc(request.query.id).set(request.body)
                    .then(()=> {
                        return response.send(
                            "Document sucessfully written - " +
                            "updated!"
                        )
                    })
                    .catch((error)=> {
                        return response.send(
                            "Error writing document: " + error
                        )
                    })
            } else {
                return response.send(
                    "Sub-collection other than 'Counter' is not supported."
                )
            }
        } else {
            return response.send(
                "ID is not specified. Cannot update without ID."
            )
        }
    } else {
        return response.send(
            "Body is empty. Cannot update without body."
        )
    }
})

app.delete("/RequestDelete", (request, response) => {    
    if(request.query.id != null) {
        if (request.query.subCollection == null) {
            db.collection("Request").doc(request.query.id).delete()
            .then(()=> {
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
        } else if (request.query.subCollection === "Counter") {
            db.collection("Request").doc(request.query.id).collection("Counter").delete()            
            .then(() => {
                return response.send(
                    "Document succesfully written - updated!"
                )
            })
            .catch((error) => {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } else {
            return response.send(
                "Deleting anything but 'Counter' subcollection is not supported."
            )
        }
    } else {
        return response.send(
            "Cannot delete document."
        )
    }
})

// 2.b

app.get('/Request_with_Subcollection', async (request, response) => {
    if (request.query.id == null) {
        let res = []
        db.collection('Request').get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    res.push(document)
                })
                return response.send(res)
            })
            .catch((error) => {
                return response.send(
                    "Error getting documents: " + error
                )
            })
    } else {
        if (request.query.subCollection === 'undefined') {
            let docRef = db
                .collection('Request')
                .doc(request.query.id)
            docRef.get()
                .then((doc) => {
                    if ( doc.data() != null) {
                            let document = {
                            id: doc.id,
                            data: doc.data()
                        }
                        return response.send(document)
                    } else {
                        let error = (
                            "Error getting document " +
                            id +
                            ": The document is undefined"
                        )
                        return response.send(error)
                    }
                })
                .catch((error) => {
                    return response.send(error)
                })
        } else {
            let res = []
            let cRef = db
                .collection('Request')
                .doc(request.query.id)
                .collection('Counter')
            cRef.get()
                .then((querySnapshot) => {
                    querySnapshot.forEach((doc) => {
                        let document = {
                            id: doc.id,
                            data: doc.data()
                        }
                        res.push(document)
                    })
                    return response.send(res)
                })
                .catch((error) => {
                    return response.send(
                        "Error getting documents " +
                        "of subcollection: " +
                        error
                    )
                })
            }
        }
})

app.get('/user_with_Subcollection', async (request, response) => {
        if (request.query.id == null) {
            let res = []
            db.collection('user').get()
                .then((querySnapshot) => {
                    querySnapshot.forEach((doc) => {
                        let document = {
                            id: doc.id,
                            data: doc.data()
                        }
                        res.push(document)
                    })
                    return response.send(res)
                })
                .catch((error) => {
                    return response.send(
                        "Error getting documents: " + error
                    )
                })
        } else {
            if (request.query.subCollection === 'undefined') {
                let docRef = db
                    .collection('user')
                    .doc(request.query.id)
                docRef.get()
                    .then((doc) => {
                        if ( doc.data() != null) {
                                let document = {
                                id: doc.id,
                                data: doc.data()
                            }
                            return response.send(document)
                        } else {
                            let error = (
                                "Error getting document " +
                                id +
                                ": The document is undefined"
                            )
                            return response.send(error)
                        }
                    })
                    .catch((error) => {
                        return response.send(error)
                    })
            } else { if(request.query.subCollection === 'BankCard')
                {let res = []
                let cRef = db
                    .collection('Request')
                    .doc(request.query.id)
                    .collection('BankCard')
                cRef.get()
                    .then((querySnapshot) => {
                        querySnapshot.forEach((doc) => {
                            let document = {
                                id: doc.id,
                                data: doc.data()
                            }
                            res.push(document)
                        })
                        return response.send(res)
                    })
                    .catch((error) => {
                        return response.send(
                            "Error getting documents " +
                            "of subcollection: " +
                            error
                        )
                    })}
                    else if(request.query.subCollection === 'Review') {
                        let res = []
                        let cRef = db
                            .collection('Request')
                            .doc(request.query.id)
                            .collection('BankCard')
                        cRef.get()
                            .then((querySnapshot) => {
                                querySnapshot.forEach((doc) => {
                                    let document = {
                                        id: doc.id,
                                        data: doc.data()
                                    }
                                    res.push(document)
                                })
                                return response.send(res)
                            })
                            .catch((error) => {
                                return response.send(
                                    "Error getting documents " +
                                    "of subcollection: " +
                                    error
                                )
                            })  
                    }
                }
            }
})*/
//http://localhost:3000/usersCreate
app.get("/usersRead", (request, response) => {
    let res = []
    if (request.query.id == null){
        db.collection("users").get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    res.push(document)
                })
                return response.send(res)
            })
            .catch(function (error){
                return response.send("Error getting docs: " + error)
            })
        } else {
            var docRef = db.collection("users").doc(request.query.id)
            docRef.get()
                .then((doc) => {
                    if (doc.data() != null){
                        //console.log("doc", doc.data())
                        let document = {
                            id: doc.id,
                            data: doc.data()
                        }
                        return response.send(document)
                    } else {
                        return response.send(
                            "Error getting document" +
                            request.query.id + 
                            ": The document is undefined"
                        )
                    }
                })
                .catch(function (error){
                    return response.send(
                        "Error getting document " + 
                        request.query.id +
                        ": " + error
                    )
                })
        }
})

app.post("/usersCreate", (request,response) => {
    if(Object.keys(request.body).length > 0) {
        db.collection("users").doc().set(request.body)
            .then(function() {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch(function(error) {
                return response.send(
                    "Error writing document: " + error
                )
            })
    } else {
        return response.send (
            "No post data for new document. " +
            "A new document is not created!"
        )
    }

})

app.put("/usersUpdate", (request, response) => {
    if(Object.keys(request.body).length){
        if(request.query.id != null){
            db.collection("users")
                .doc(request.query.id)
                .update(request.body)
                .then(function (){
                    return response.send(
                        "Document sucessfully written - " +
                        "updated!"
                    )
                })
                .catch(function(error){
                    return response.send(
                        "Error writing document: " + error
                    )
                })
        } else {
            return response.send(
                "A parameter id is not set. " +
                "A document is not updated!"
            )
        }
    } else {
        return response.send(
            "No post data for new document. " + 
            "A document  is not updated!"
        )
    }
})

app.delete("/usersDelete",(request, response) => {
    if(request.query.id != null) {
        db.collection("users").doc(request.query.id).delete()
            .then(function(){
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch(function(error) {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
    } else {
        return response.send(
            "A parameter id is not set. " + 
            "A document is not deleted!"
        )
    }
})
//get, put, post, delete za podkolekciju 
app.get('/usersSubcollectionRead', async (request,response) => {
    if(typeof request.query.id === 'undefined') {
        let res = []
        db.collection('users').get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    res.push(document)
                })
                return response.send(res)
            })
            .catch(function (error) {
                return response.send("Error getting documents: " + error)
            })
    } else {
        if(typeof request.query.subCollection === 'undefined')
        {
        let docRef = db.collection('users').doc(request.query.id)
        docRef.get()
            .then((doc) => {
                if (typeof doc.data() !== 'undefined'){
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    return response.send(document)
                } else {
                    let error = (
                    "Error getting document " +
                    id +
                    ": The document is undefined"
                    )
                    return response . send ( error )
                    }
            })
            .catch(function (error) {
                        return response.send(error)
            }
            )
    } else {
        let res = []
        let cRef = db.collection('users').doc(request.query.id).collection(request.query.subCollection)
        cRef.get()
            .then((querySnapshot) => {
                querySnapshot.forEach((doc) => {
                    let document = {
                        id: doc.id,
                        data: doc.data()
                    }
                    res.push(document)
                })
                return response.send(res)
            })
            .catch(function (error) {
                return response.send(
                    "Error getting documents" +
                    " of subcollection: " + 
                    error
                )
            })
    }
    }
})

// .../usersSubcollectionCreate?id=fg4weadsfgft5e4we&subCollection=UserBenefits
app.post("/usersSubcollectionCreate", (request,response) => {
    if(Object.keys(request.body).length > 0) {
        if (typeof request.query.id === 'undefined') {
            db.collection("users").doc().set(request.body)
            .then(function() {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch(function(error) {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } else {
            if(typeof request.query.subCollection === 'undefined'){
                db.collection("users").doc(request.query.id)
                    .set(request.body)
                .then(function() {
                    return response.send(
                        "Document succesfully written - created!"
                    )
                })
                .catch(function(error) {
                    return response.send(
                        "Error writing document: " + error
                    )
                })
            } else {
                let subRef = db.collection("users").doc(request.query.id).collection(request.query.subCollection).doc()
                subRef.set(request.body)
                .then(function() {
                    return response.send("Document succesfully written - created!")
                })
                .catch(function(error) {
                    return response.send(
                        "Error writing document: " + error 
                    )
                })
            }
    } }
})

app.put("/usersSubcollectionUpdate", (request, response) => {
    if(Object.keys(request.body).length > 0) {
        if (typeof request.query.id !== 'undefined'&& typeof request.query.subCollection !== 'undefined') {
            db.collection("users").doc(request.query.id).collection("UserBenefits").doc(request.query.subCollection).update(request.body)
            .then(function() {
                return response.send(
                    "Document succesfully written - created!"
                )
            })
            .catch(function(error) {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } else {
            return response.send(
                    "Error writing document: " + error
                )
            }
        }
    })
/*
app.delete("/usersSubcollectionDelete", (request, response) => {    
    if(request.query.id != null) {
        if (request.query.subCollection == null) {
            db.collection("users").doc(request.query.id).collection(request.query.subCollection).doc(request.query.id).delete()
            .then(function(){
                return response.send(
                    "Document succesfully deleted!"
                )
            })
            .catch(function(error) {
                return response.send(
                    "Error removing document: " + error 
                )
            }) 
        } else {
            db.collection("users").doc(request.query.id).collection("UserBenefits").delete()            
            .then(function() {
                return response.send(
                    "Document succesfully written - updated!"
                )
            })
            .catch(function(error) {
                return response.send(
                    "Error writing document: " + error
                )
            })
        } 
}
})*/

app.listen(3000, () => {
    console.log("Server running on port 3000");
});


